@extends('layout')

@section("content")
    <h1>Formations</h1>
    <ul>
        @if (sizeof($formations) > 0)
            @foreach ($formations as $formation)
                <li><a href="{{route("formationDetails", $formation->id)}}">{{$formation->nom}}</a></li>
            @endforeach
        @else
        <p>Pas d'articles</p>
        @endif
    </ul>
@endsection