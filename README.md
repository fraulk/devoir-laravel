# Projet Laravel

## About

Projet Laravel is an online learning platform for the Sigma corporation, made with Laravel 8 and a MySQL server.  

Only the list of every formations, and details about them has been done.  

## Installation

```shell
php artisan migrate
php artisan db:seed
php artisan serve
```

### MCD

![MCD](./mcd_draw_io.png)