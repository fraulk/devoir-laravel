<?php

namespace Database\Factories;

use App\Models\Formation;
use Illuminate\Database\Eloquent\Factories\Factory;

class FormationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Formation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nom' => $this->faker->name(),
            'description' => $this->faker->sentence(10),
            'image_couv' => '',
            'chapters' => $this->faker->sentence(2),
            'price' => $this->faker->numberBetween(5, 15),
            'type' => 'utilisateur'
        ];
    }
}
